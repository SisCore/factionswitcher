local funcIsProcessing = false
local reptable = {}
local allowFactionCount = 0;

local itemFaction = {
	id = 0,
	index = 0,
	name = "",
	value = 0
}

local eventFrame = CreateFrame("FRAME", "FactionSwitcherEventsFrame");
eventFrame:RegisterEvent("PLAYER_LOGIN");
eventFrame:RegisterEvent("PLAYER_LOGOUT");
eventFrame:RegisterEvent("CHAT_MSG_COMBAT_FACTION_CHANGE");

eventFrame:SetScript("OnEvent", function(self, event, ...)
	if (event == "PLAYER_LOGIN") then
		allowFactionCount = GetNumFactions();
		C_Timer.After(20, function () GetAllFactionsInfo(allowFactionCount); end)
	elseif (event == "PLAYER_LOGOUT") then 
		reptable = {};
		allowFactionCount = 0;
	elseif (event == "CHAT_MSG_COMBAT_FACTION_CHANGE") then 
		if not funcIsProcessing then 
        GoFactionUpdate()
    end
	end
   local factionCount = GetNumFactions();
   if allowFactionCount < factionCount then
		GetAllFactionsInfo(factionCount);
   end
end)

function GetAllFactionsInfo(factionCount)
	if (factionCount == nil) then 
		factionCount = allowFactionCount;
	end
	
	reptable = {};
	for x=1, factionCount, 1 do
		local name, description, standingID, barMin, barMax, barValue, atWarWith, canToggleAtWar, isHeader, isCollapsed, hasRep, isWatched, isChild, factionID, hasBonusRepGain, canBeLFGBonus = GetFactionInfo(x);
		
		if (hasRep or not isHeader) then
			local faction = {
				id = factionID,
				index = x,
				name = name,
				value = barValue,
				maxValue = barMax,
				isFactionParagon = false,
				paragonValue = 0
			};
			
			if (C_Reputation.IsFactionParagon(faction.id)) then
				faction.isFactionParagon = true;
				local currentValue, threshold, rewardQuestID, hasRewardPending, tooLowLevelForParagon = C_Reputation.GetFactionParagonInfo(faction.id);
				faction.paragonValue = currentValue;
			end
			
			table.insert(reptable, faction);
		end
	end
end

function GoFactionUpdate()
    funcIsProcessing = true
	for x=1, table.getn(reptable), 1 do
		local faction = reptable[x];

		if (faction.value < faction.maxValue) then 
			local name, description, standingID, barMin, barMax, barValue, atWarWith, canToggleAtWar, isHeader, isCollapsed, hasRep, isWatched, isChild, factionID, hasBonusRepGain, canBeLFGBonus = GetFactionInfoByID(faction.id);
			if (faction.value ~= barValue) then 
				faction.value = barValue;
				SetWatchedFactionIndex(faction.index);
				break;
			end
		else
			if (faction.isFactionParagon) then 
				local currentValue, threshold, rewardQuestID, hasRewardPending, tooLowLevelForParagon = C_Reputation.GetFactionParagonInfo(faction.id);
				if (faction.paragonValue ~= currentValue) then
					faction.paragonValue = currentValue
					SetWatchedFactionIndex(faction.index);
					break;
				end
			end
		end 
	end
	
    funcIsProcessing = false
end